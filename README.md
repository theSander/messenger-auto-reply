# Facebook Messenger Auto Reply (unofficial)

This project is meant to enable simple auto-reply functionality for Facebook user accounts using
Messenger. It uses the facebook-chat-api (https://github.com/Schmavery/facebook-chat-api).

## Usage

To set up auto-reply locally, run
```bash
# install dependencies
npm install
# initialize state file
cp state/default-messenger-auto-reply-state.json state/messenger-auto-reply-state.json
# register your account
./messenger-auto-reply.js -u <your_facebook_login> -r <your_reply_text>
# start the auto-reply service for all registered users
./messenger-auto-reply.js --start
```

The options for running the script are as follows:
```
SYNOPSIS
	messenger-auto-reply [-h|--help] [-v|--version] [--start] [-l|--list-users]
	                     [--default-reply [<message>]] [-u|--user <email>]
	                     [-r|--reply-message <message>] [--log-out]

OPTIONS
	-h, --help
		Show this help.
	-v, --version
		Show version.
	--start
		Starts auto-replying for all registered users.
	-l, --list-users
		List all registered users.
	--default-reply [<message>]
		Default reply message for new users. If no <message> is given, will print out the current default and exit.
	-u, --user <email>
		Authenticate and register a user with email address <email>. Will prompt for a password.
		This option is required when using user-specific options such as --reply-message or --log-out.
	-r, --reply-message [<message>]
		Reply message for the user passed with -u. If a <message> is given, sets the reply to <message>, otherwise prints the current reply.
	--log-out
		Logs out and forgets the user passed with -u.
```
`DISCLAIMER`: While testing, it has occurred that Facebook blocked my account. It was very straightforward to fix this, and
it only happened once for each account I tested with. However, Facebook forced me to pick a new password, different from
the old one. The facebook-chat-api, and therefore this application, registers such a case as two-factor-authentication.

## Installation
To install, run `sudo install.sh`. This should provide a systemd service you can enable with
`sudo systemctl enable messenger-auto-reply` and start with `sudo systemctl start messenger-auto-reply`
### Detailed installtion information
The installation creates a user `facebook` and initializes the state file (in `/var/lib/facebook/`) with
640 permissions, so that the cookies stored in it are only accessible from the application, and users
with elevated priviliges.
As a result, messenger-auto-reply should be run as the `facebook` user to be able to access the state file.

`DISCLAIMER`: system-wide installation is not working yet. I am still looking into how to fix this.
